# mk-lineage

**WARNING** *This is an unmaintined project and needs some cleaning.*

### Dependencies:

```
adb
fastboot
```

For void-linux users, run the following in the meta directory:
```sh
chmod +x *.sh
./install.sh
```


### Install Files to data/ 

#### Lineage OS

Select your [device](https://download.lineageos.org/) and download the 
latest build for your device.

It is adviced to optionally verify the install with `sha256sum` and 
compare with the sha256 checksum for the build you downloaded.
If they do not match, download it again.

Finally, place the file in `mk-lineage/data/` (this is a relative path 
of course).


#### Gapps

Download the version of [open 
gapps](https://opengapps.org/?api=9.0&variant=nano) you want / need.

I personally use `nano`. Here's a [comparison 
chart](https://github.com/opengapps/opengapps/wiki/Package-Comparison) 
to help you decide.

Your device is mostlikely `arm64`.

For Lineage 16, use Android 9.
For Lineage 17, use Android 10.

It is adviced to optionally verify the install with `sha256sum` and 
compare with the sha256 checksum for the build you downloaded.
If they do not match, download it again.

Finally, place the file in `mk-lineage/data/` (this is a relative path 
of course).


#### Root/Jailbreaking

Download the latest version of 
[addonsu](https://download.lineageos.org/extras), and again probably 
install for `arm64`.

It is adviced to optionally verify the install with `sha256sum` and 
compare with the sha256 checksum for the build you downloaded.
If they do not match, download it again.

Finally, place the file in `mk-lineage/data/` (this is a relative path 
of course).


#### Firmware

Find your device on the [lineage 
wiki](https://wiki.lineageos.org/devices/) and go to the corresponding 
page.
There'll be a page called "Installation" with a subsection called 
"Updating firmware".  Go there and follow the link to the download 
page.

Make sure to download the correct and latest file listed on the page. 
Do not fall for any of the ads.

It is adviced to optionally verify the install with `sha1sum` and 
compare with the sha1 checksum for the build you downloaded.
If they do not match, download it again.

Finally, place the file in `mk-lineage/data/` (this is a relative path 
of course).


#### Custom Recovery

Find your device on the [twrp webpage](https://twrp.me/Devices/) and 
go to the corresponding page.
Go there and follow the links to the download page.

Download the corresponding .img file.

It is adviced to optionally verify the install with `sha256sum` and 
compare with the sha256 checksum for the build you downloaded.

Finally, place the file in `mk-lineage/data/` (this is a relative path 
of course).



### Google Applications and Services

*This is what I use after installing the nano version of open gapps; 
this is only for recommendation purposes*

**Install separately:**
- Google Zhuyin Input (Zhuyin)

**Remove:**
- Google Calendar Sync (CalSync)
- Dialer Framework (DialerFramework)
- Google Package Installer (PackageInstallerGoogle)
- Google Markup (Markup)
- Google Digital Wellbeing (Wellbeing)
- Google Sounds (Sounds)
