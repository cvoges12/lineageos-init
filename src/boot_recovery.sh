#!/usr/bin/env bash
( while :; do
    for c in / - \\ \|;
    do
        printf '%s\b' "$c"; sleep .1;
    done;
done ) &
fastboot boot data/twrp*.img   # may not work
{ printf '\n'; kill $! && wait $!; } 2>/dev/null
