#!/usr/bin/env bash
( while :; do
    for c in / - \\ \|;
    do
        printf '%s\b' "$c"; sleep .1;
    done;
done ) &
adb install data/OpenWeatherProvider*.apk
{ printf '\n'; kill $! && wait $!; } 2>/dev/null
