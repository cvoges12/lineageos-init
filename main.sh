#!/usr/bin/env bash


newline(){
    echo ""
}

title(){
    newline
    newline
    echo "### $1 ###"
    newline
}

user_action(){
    echo $1
    read -n 1 -r -s -p "Press any key to continue..."
    newline
}

question(){
    read -n 1 -r -s -p "$1" q
    echo "$q"
}

usb_debugging(){
    user_action "Enable Developer Mode"
    user_action "Enable USB Debugging"
    user_action "Plug in the device to the PC"
}

oem_and_verify(){
    user_action "Unlock OEM in Developer Options"
    newline
    echo "Booting into bootloader..."
    source src/bootloader.sh
    newline
    echo "If the device did not boot,"
    user_action "Power off and hold <Volume Up> + <Power>"
    newline
    source src/verify.sh
    user_action "Verify your device above"
}

unlock_bootloaders(){
    echo "Is your bootloader unlocked? (Y/n)"
    a=$(question "If unsure, respond with: y")
    if [[ $a == "n" ]] || [[ $a == "N" ]]; then
        newline
        oem_and_verify
        newline
        echo "Unlocking bootloader..."
        source src/unlock.sh
        newline
        echo "If the device did not reboot automatically,"
        user_action "Reboot manually"

        #b/c unlocking the bootloader wipes everything
        usb_debugging
    fi
    newline
}



boot_recovery(){
    echo "Attempting to boot into recovery..."
    source src/boot_recovery.sh
    newline
    echo "If the device did not boot,"
    user_action "Power off and hold <Volume Down> + <Power>"
    newline
}

boot_new_recovery(){
    echo "Flashing recovery..."
    source src/recovery.sh
    boot_recovery
}


setup_sideload(){
    user_action "Select Advanced > ADB Sideload > begin sideload"
}

firmware(){
    echo "Is your firmware up to date? (Y/n)"
    a=$(question "If unsure, respond with: y")
    if [[ $a == "n" ]] || [[ $a == "N" ]]; then
        newline
        setup_sideload
        newline
        echo "Flashing firmware..."
        source src/firmware.sh

        # set up for Stage 4
        boot_recovery
    fi
    newline
}

lineage(){
    echo "Installing LineageOS..."
    source src/lineage.sh
}

gapps(){
    a=$(question "Do you want to install Google Apps? (y/N)")
    if [[ $a == "y" ]] || [[ $a == "Y" ]]; then
        newline
        setup_sideload
        newline
        source src/gapps.sh
    fi
    newline
}

rooted(){
    a=$(question "Do you want this device to be rooted? (y/N)")
    if [[ $a == "y" ]] || [[ $a == "Y" ]]; then
        newline
        setup_sideload
        newline
        source src/rooted.sh
    fi
    newline
}

weather(){
    a=$(question "Do you want this device to have a weather provider? (y/N)")
    if [[ $a == "y" ]] || [[ $a == "Y" ]]; then
        setup_sideload
        source src/weather.sh
    fi
    newline
}

wipe(){
    user_action "Tap Wipe > Format Data > Swipe to Format"
    newline
    echo "Tap Wipe > Advanced Wipe,"
    echo "Select Cache and System partitions,"
    user_action "And finally, Swipe to Wipe"
    newline
    user_action "Reboot System"
    newline
}

echo "Ctrl+c at any time to exit"
newline

echo "Make sure the files in data/ are updated, including:"
echo "  - addonsu"
echo "  - firmware"
echo "  - lineage"
echo "  - open gapps"
echo "  - open weather provider"
echo "  - twrp"
newline

user_action "Install required packages"
newline

#imagine trying to do this without your device plugged in
usb_debugging

title "Stage 1: Unlocking Bootloader"
unlock_bootloaders

title "Stage 2: Install Custom Recovery"
oem_and_verify
newline
boot_new_recovery

title "Stage 3: Updating Firmware"
firmware

title "Stage 4: Install LineageOS"
wipe
boot_new_recovery
newline
setup_sideload
lineage
gapps
#weather
rooted

title "Finished"
source src/reboot.sh
echo "Reboot the device manually if needed"
